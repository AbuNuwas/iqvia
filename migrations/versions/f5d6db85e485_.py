"""empty message

Revision ID: f5d6db85e485
Revises: 1b9991953b96
Create Date: 2019-05-26 17:09:18.128217

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f5d6db85e485'
down_revision = '1b9991953b96'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('contact', sa.Column('created', sa.DateTime(), nullable=True))
    op.add_column('contact', sa.Column('updated', sa.DateTime(), nullable=True))
    op.create_index(op.f('ix_contact_created'), 'contact', ['created'], unique=False)
    op.create_index(op.f('ix_contact_updated'), 'contact', ['updated'], unique=False)
    op.add_column('email', sa.Column('created', sa.DateTime(), nullable=True))
    op.add_column('email', sa.Column('updated', sa.DateTime(), nullable=True))
    op.create_index(op.f('ix_email_created'), 'email', ['created'], unique=False)
    op.create_index(op.f('ix_email_updated'), 'email', ['updated'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_email_updated'), table_name='email')
    op.drop_index(op.f('ix_email_created'), table_name='email')
    op.drop_column('email', 'updated')
    op.drop_column('email', 'created')
    op.drop_index(op.f('ix_contact_updated'), table_name='contact')
    op.drop_index(op.f('ix_contact_created'), table_name='contact')
    op.drop_column('contact', 'updated')
    op.drop_column('contact', 'created')
    # ### end Alembic commands ###
