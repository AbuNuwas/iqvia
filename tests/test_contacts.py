import copy
import json

from app.models.models import Contact, Email
from tests.base import TestBase

CONTACTS = [
    {
        'emails': ['example1@example.com'],
        'first_name': 'example1',
        'surname': 'example1',
        'username': 'example1',
    },
    {
        'emails': ['example2@example.com'],
        'first_name': 'example2',
        'surname': 'example2',
        'username': 'example2',
    },
]


def make_db_contact_record_comparable(contact):
    comparable_fields = ['emails', 'first_name', 'surname', 'username']
    return {key: value for key, value in contact.items()
            if key in comparable_fields}


class TestContacts(TestBase):

    def setup(self):
        for contact in CONTACTS:
            self.create_new_contact(contact)

    @staticmethod
    def create_new_contact(payload):
        data = copy.deepcopy(payload)
        emails = data.pop('emails')
        contact = Contact(**data).save()
        for email in emails:
            Email(address=email, contact_id=contact.id).save()
        return contact

    def test_get_contacts(self, client):
        response = client.get('/contacts')
        comparable = [make_db_contact_record_comparable(contact)
                      for contact in response.json['contacts']]
        assert comparable == CONTACTS

    def test_get_contact_by_username(self, client):
        response = client.get('/contacts?username=example1')
        comparable = make_db_contact_record_comparable(response.json['contacts'])
        assert comparable == CONTACTS[0]

    def test_get_contact_by_email(self, client):
        response = client.get('/contacts?email=example1@example.com')
        comparable = make_db_contact_record_comparable(response.json['contacts'])
        assert comparable == CONTACTS[0]

    def test_get_contact_by_illegal_parameter_fails(self, client):
        response = client.get('/contacts?surname=example1')
        assert response.status_code == 400

    def test_create_contact(self, client):
        payload = {
            'emails': ['example3@example.com'],
            'first_name': 'example3',
            'surname': 'example3',
            'username': 'example3',
        }
        response = client.post('/contacts', data=json.dumps(payload),
                               headers={'Content-Type': 'application/json'})
        comparable = make_db_contact_record_comparable(response.json)
        assert comparable == payload

    def test_create_contact_emails_is_not_array_fails(self, client):
        payload = {
            'emails': 'example3@example.com',
            'first_name': 'example3',
            'surname': 'example3',
            'username': 'example3',
        }
        response = client.post('/contacts', data=json.dumps(payload),
                               headers={'Content-Type': 'application/json'})
        assert response.status_code == 400

    def test_create_contact_existing_username_fails(self, client):
        payload = {
            'emails': ['example1@example.com'],
            'first_name': 'example1',
            'surname': 'example1',
            'username': 'example1',
        }
        response = client.post('/contacts', data=json.dumps(payload),
                               headers={'Content-Type': 'application/json'})
        assert response.status_code == 409

    def test_create_contact_illegal_payload_fails(self, client):
        payload = {
            'emails': ['example1@example.com'],
            'first_name': 'example1',
            'surname': 'example1',
            'username': 'example1',
            'hobbies': 'exampling',
        }
        response = client.post('/contacts', data=json.dumps(payload),
                               headers={'Content-Type': 'application/json'})
        assert response.status_code == 400

    def test_create_contact_missing_field_fails(self, client):
        payload = {
            'emails': ['example1@example.com'],
            'surname': 'example1',
            'username': 'example1',
        }
        response = client.post('/contacts', data=json.dumps(payload),
                               headers={'Content-Type': 'application/json'})
        assert response.status_code == 400

    def test_update_contact(self, client):
        contact = self.create_new_contact({
            'emails': ['email@example.com'],
            'first_name': 'first_name',
            'surname': 'surname',
            'username': 'username',
        })
        update_payload = {
            'emails': ['new_email@example.com'],
            'first_name': 'new_first_name',
            'surname': 'new_surname',
            'username': 'new_username',
        }
        response = client.put(
            f'/contacts/{contact.id}', data=json.dumps(update_payload),
            headers={'Content-Type': 'application/json'}
        )
        comparable = make_db_contact_record_comparable(response.json)
        contact.delete()
        assert comparable == update_payload

    def test_update_contact_reset_emails(self, client):
        contact = self.create_new_contact({
            'emails': ['email@example.com'],
            'first_name': 'first_name',
            'surname': 'surname',
            'username': 'username',
        })
        update_payload = {
            'emails': ['new_email@example.com', 'another_email@example.com'],
            'first_name': 'new_first_name',
            'surname': 'new_surname',
            'username': 'new_username',
        }
        response = client.put(
            f'/contacts/{contact.id}', data=json.dumps(update_payload),
            headers={'Content-Type': 'application/json'}
        )
        comparable = make_db_contact_record_comparable(response.json)
        contact.delete()
        assert comparable == update_payload

    def test_update_contact_existing_username_fails(self, client):
        contact = self.create_new_contact({
            'emails': ['email@example.com'],
            'first_name': 'first_name',
            'surname': 'surname',
            'username': 'username',
        })
        update_payload = {
            'emails': ['new_email@example.com'],
            'first_name': 'new_first_name',
            'surname': 'new_surname',
            'username': 'example1',
        }
        response = client.put(
            f'/contacts/{contact.id}', data=json.dumps(update_payload),
            headers={'Content-Type': 'application/json'}
        )
        contact.delete()
        assert response.status_code == 409

    def test_update_contact_illegal_payload_fails(self, client):
        contact = self.create_new_contact({
            'emails': ['email@example.com'],
            'first_name': 'first_name',
            'surname': 'surname',
            'username': 'username',
        })
        update_payload = {
            'emails': ['new_email@example.com'],
            'first_name': 'new_first_name',
            'surname': 'new_surname',
            'username': 'new_username',
            'hobbies': 'exampling'
        }
        response = client.put(
            f'/contacts/{contact.id}', data=json.dumps(update_payload),
            headers={'Content-Type': 'application/json'}
        )
        contact.delete()
        assert response.status_code == 400

    def test_delete_contact(self, client):
        contact = self.create_new_contact({
            'emails': ['email@example.com'],
            'first_name': 'first_name',
            'surname': 'surname',
            'username': 'username',
        })
        response = client.delete(f'/contacts/{contact.id}')
        assert response.status_code == 200
