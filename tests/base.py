import os
import tempfile

import pytest

import app


class TestBase:

    @pytest.fixture
    def client(self):
        db_fd, db_file = tempfile.mkstemp()
        app.app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{db_file}'
        app.app.config['TESTING'] = True
        client = app.app.test_client()

        with app.app.app_context():
            app.db.create_all()

        yield client

        os.close(db_fd)
        os.unlink(db_file)
