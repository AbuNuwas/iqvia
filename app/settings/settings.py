import os
from pathlib import Path

BASEDIR = Path(__file__).parent.parent
DEFAULT_DATABASE_URL = f'sqlite:///{Path(BASEDIR / "app.db").as_posix()}'


class Config:
    SQLALCHEMY_DATABASE_URI = os.environ.get(
        'DATABASE_URL', DEFAULT_DATABASE_URL
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    CELERY_BROKER_URL = os.environ.get(
         'CELERY_BROKER_URL', 'redis://localhost:6379/0'
    )
