CELERY_IMPORTS = ('app.tasks.tasks',)
CELERY_TASK_RESULT_EXPIRES = 30
CELERY_TIMEZONE = 'UTC'

CELERY_ACCEPT_CONTENT = ['json', 'msgpack', 'yaml']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

CELERYBEAT_SCHEDULE = {
    'test-celery': {
        'task': 'app.tasks.tasks.create_contact',
        'schedule': 15.0,
    },
    'see-you-in-ten-seconds-task': {
        'task': 'app.tasks.tasks.delete_old_records',
        'schedule': 60.0
    }
}
