import datetime

from app import db
from app.models.base_model import BaseModel


class Contact(BaseModel):
    created = db.Column(db.DateTime, index=True, default=datetime.datetime.utcnow)
    emails = db.relationship('Email', backref='contact', cascade='all,delete')
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(120))
    surname = db.Column(db.String(120))
    updated = db.Column(db.DateTime, index=True, default=datetime.datetime.utcnow)
    username = db.Column(db.String(120), index=True, unique=True)

    UPDATABLE_FIELDS = ['first_name', 'surname', 'username']

    def __repr__(self):
        return f'<Contact {self.username}>'


class Email(BaseModel):
    address = db.Column(db.String(120), index=True, unique=True)
    contact_id = db.Column(db.Integer, db.ForeignKey('contact.id'))
    created = db.Column(db.DateTime, index=True, default=datetime.datetime.utcnow)
    id = db.Column(db.Integer, primary_key=True)
    updated = db.Column(db.DateTime, index=True, default=datetime.datetime.utcnow)

    UPDATABLE_FIELDS = ['address']

    def __repr__(self):
        # Return just the address to facilitate its representation during the
        # response marshalling
        return self.address
