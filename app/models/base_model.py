from app import db


class BaseModel(db.Model):
    """
    Base model providing basic methods that can be used by all database models.
    """

    __abstract__ = True

    UPDATABLE_FIELDS = []

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self

    def update(self, **data):
        for field in self.UPDATABLE_FIELDS:
            if field in data:
                setattr(self, field, data[field])
        db.session.commit()
        return self

    def delete(self):
        db.session.delete(self)
        db.session.commit()