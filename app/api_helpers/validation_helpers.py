from werkzeug.exceptions import BadRequest


def validate_payload(api_definition_model, payload):
    """
    The `api.expect` method from `flask-restplus` checks for missing parameters
    in the payload. For additional validation, this function checks that no
    more fields than those which are allowed by the spec are sent to the
    backend. It also ensures that readOnly values are not sent to the backend.
    """

    allowed_keys = {
        field for field, value in api_definition_model.items()
        if not value.readonly
    }
    if set(payload.keys()) - allowed_keys:
        raise BadRequest('Payload is not correctly formed.')


def validate_query_parameters(query_parameters, allowed_parameters):
    """
    Checks that only allowed query parameters are present in the URL.
    """

    if set(query_parameters.keys()) - set(allowed_parameters):
        raise BadRequest('Illegal query parameters in url.')
