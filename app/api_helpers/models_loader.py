from dataclasses import dataclass
from flask_restplus import fields


fields_mapper = {
    'boolean': fields.Boolean,
    'float': fields.Float,
    'integer': fields.Integer,
    'array': fields.List,
    'string': fields.String,
}


@dataclass
class Field:
    """
    Dataclass representing a field from an API object. It allows to transform
    the field definition from the API spec into the object expected by
    `flask-restplus` for request validation and response marshalling.
    """
    field_type: str
    name: str
    read_only: bool
    required: bool

    # only required when field_type is an array
    items_type: str = None

    def __post_init__(self):
        self.field_type_class = fields_mapper[self.field_type]

    def get_object(self):
        parameters = {
            'required': self.required,
            'readonly': self.read_only
        }
        if self.items_type:
            parameters['cls_or_instance'] = fields_mapper[self.items_type]()
        return self.field_type_class(**parameters)


def load_model(api, model_title, definition):
    """
    Parses the fields definitions of an API object and transforms them into
    objects that can be used by `flask-restplus` for request validation and
    response marshalling.
    """

    model_fields = {}
    for key, value in definition['properties'].items():
        model_field = Field(
            field_type=value['type'], name=key,
            read_only=value.get('readOnly', False),
            required=key in definition.get('required', []),
            items_type=value.get('items', {}).get('type')
        )
        model_fields[key] = model_field.get_object()
    return api.model(model_title.title(), model_fields)

