from datetime import datetime, timedelta
from uuid import uuid4

from app import celery
from app.models.models import Contact, Email


@celery.task
def create_contact():
    unique_record = uuid4()
    contact_data = {
        'emails': [f'{unique_record}@example.com'],
        'first_name': f'{unique_record}',
        'surname': f'{unique_record}',
        'username': f'{unique_record}',
    }
    emails = contact_data.pop('emails')
    contact = Contact(**contact_data).save()
    for email in emails:
        Email(address=email, contact_id=contact.id).save()
    print(f'Created contact {contact}')


@celery.task
def delete_old_records():
    old_records = Contact.query.filter(
        Contact.created < datetime.utcnow() - timedelta(minutes=1)
    )
    for record in old_records:
        print(f'Deleting record {record}...')
        record.delete()
