import yaml
from flask import request
from flask_restplus import Resource, abort

from app import api, api_namespace
from app.api_helpers.models_loader import load_model
from app.api_helpers.validation_helpers import validate_payload, \
    validate_query_parameters
from app.models.models import Contact, Email
from app.settings.settings import BASEDIR


spec_dict = yaml.safe_load((BASEDIR.parent / 'api.yaml').read_text())
api_definitions = spec_dict['definitions']
contact_definition = load_model(api=api, model_title='Contact',
                                definition=api_definitions['Contact'])


@api.route('/contacts')
class ContactsView(Resource):

    @api_namespace.marshal_with(contact_definition, envelope='contacts')
    def get(self):
        if request.args:
            validate_query_parameters(request.args, {'username', 'email'})
            if request.args.get('username'):
                return Contact.query.filter_by(
                    username=request.args['username']
                ).first()
            else:
                email = Email.query.filter_by(
                    address=request.args['email']
                ).first()
                if email:
                    return email.contact
                else:
                    return {}
        return Contact.query.all()

    @api_namespace.expect(contact_definition, validate=True)
    @api_namespace.marshal_with(contact_definition)
    def post(self):
        contact_data = api.payload
        validate_payload(contact_definition, contact_data)
        if Contact.query.filter_by(username=contact_data['username']).first():
            abort(409, '"username" already exists.')
        emails = contact_data.pop('emails')
        check_emails = [Email.query.filter_by(address=email).first()
                        for email in emails]
        for email in check_emails:
            if email:
                abort(409, '"email" already exists')
        contact = Contact(**contact_data).save()
        for email in emails:
            Email(address=email, contact_id=contact.id).save()
        return contact, 201


@api.route('/contacts/<int:contact_id>')
class ContactView(Resource):

    @api_namespace.marshal_with(contact_definition, envelope='contact')
    def get(self, contact_id):
        return Contact.query.get_or_404(contact_id)

    @api_namespace.expect(contact_definition, validate=True)
    @api_namespace.marshal_with(contact_definition)
    def put(self, contact_id):
        contact_data = api.payload
        validate_payload(contact_definition, contact_data)
        contact = Contact.query.get_or_404(contact_id)
        # if the user is trying to update the username of the contact with an
        # existing username (which isn't his), abort
        username_check = Contact.query.filter_by(username=contact_data['username']).first()  # noqa: E501
        if username_check and username_check.id != contact.id:
            abort(409, '"username" already exists.')
        emails = contact_data.pop('emails')
        check_emails = [Email.query.filter_by(address=email).first()
                        for email in emails]
        for email in check_emails:
            if email and email.contact.id != contact_id:
                abort(409, '"email" already exists')
        contact = contact.update(**contact_data)
        for email in contact.emails:
            if email not in emails:
                email.delete()
        for email in emails:
            if email not in contact.emails:
                Email(address=email, contact_id=contact.id).save()
        return contact

    @staticmethod
    def delete(contact_id):
        Contact.query.get_or_404(contact_id).delete()
        return 200
