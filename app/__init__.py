from flask import Flask
from flask_migrate import Migrate
from flask_restplus import Api
from flask_sqlalchemy import SQLAlchemy

from app.settings.settings import Config
from app.tasks.celery_factory import make_celery

app = Flask('contacts_app')
app.config.from_object(Config)
api = Api(app, version='1.0', title='Contacts API', description='Contacts API')
api_namespace = api.namespace('contacts', description='Contacts API')
db = SQLAlchemy(app)
migrate = Migrate(app, db)
celery = make_celery(app)
celery.conf.update(app.config)

from app.views import contact_views
from app.models import models
